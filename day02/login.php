<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
<div class="form">
    <div class="date">
        <?php
        date_default_timezone_set('Asia/Ho_Chi_Minh');

        $dayOfWeek = date("N");

        if ($dayOfWeek == 7) {
            $dayOfWeek = "Chủ nhật";
        }

        echo "Bây giờ là: ";
        echo date("G:i");
        echo ", thứ ";
        echo $dayOfWeek;
        echo " ngày ";
        echo date("d/m/Y");
        ?>
    </div>
    <div class="login">
    <label class="label"> Tên đăng nhập </label>
    <input type="text" class="input">
    <br>
        <br>
    <label class="label"> Mật khẩu </label>
    <input type="password" class="input">
    </div>
    <br>
    <input type="submit" class= "BT_Login" value= "Đăng nhập">
</div>
</body>
</html>