<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
<form class="form" id="form" onsubmit="return validateForm()">
    <div class="message" id="message">
        <p id="error-message" class="error-message"></p>
    </div>
    <div class="center-form">
        <div class="table">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label>
                <input type="text" id="ho_ten" class="input" required>
            </label>
        </div>
        <div class="table">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <div class="radio">
                <?php
                $gioi_tinh = array(0 => "Nam", 1 => "Nữ");
                foreach ($gioi_tinh as $key => $value) {
                    echo '<input required type="radio" id="gioi_tinh' . $key . '" name="gioi_tinh" value="' . $key . '" ' . '>';
                    echo '<label for="gioi_tinh_' . $key . '">' . $value . '</label>';
                }
                ?>
            </div>
        </div>
        <div class="table">
            <label class="label"> Phân Khoa <span class="validate">*</span> </label>
            <label>
                <select name="state" class="Department" id="Department" required>
                    <option disabled selected hidden value=""> Chọn phân khoa</option>
                    <?php
                    $khoa = array("MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                    foreach ($khoa as $key => $value) {
                        echo '<option value= "' . $key . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </label>
        </div>
        <div class="table">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label>
                <input type="date" class="Department" id="ngay_sinh" required>
            </label>
        </div>
        <div class="table">
            <label class="label"> Địa chỉ </label>
            <label>
                <input type="text" class="address">
            </label>
        </div>
        <div class="table">
            <label class="label"> Hình ảnh </label>
            <label>
                <input type="file" id="image" class="image">
            </label>
        </div>
        <div class="center-btn">
            <input type="submit" id="submit" class="submit-btn" value="Đăng Ký">
        </div>
    </div>
</form>
<script>
    document.getElementById("submit").addEventListener('click', function () {

        const hoTen = document.getElementById('ho_ten').value;
        const gioiTinh = document.querySelector('input[name="gioi_tinh"]:checked');
        const khoa = document.getElementById('Department').value;
        const ngaySinh = document.getElementById('ngay_sinh').value;
        let count = 0;
        let errorMessage = '';
        let errorFlag = false;

        if (hoTen === '') {
            errorMessage += 'Hãy nhập tên.<br>';
            errorFlag = true;
            count += 1;
        }

        if (!gioiTinh) {
            errorMessage += 'Hãy chọn giới tính.<br>';
            errorFlag = true;
            count += 1;
        }

        if (khoa === '') {
            errorMessage += 'Hãy chọn phân khoa.<br>';
            errorFlag = true;
            count += 1;
        }

        if (ngaySinh === '') {
            errorMessage += 'Hãy nhập ngày sinh.<br>';
            errorFlag = true;
            count += 1;
        }

        if (errorFlag) {
            document.getElementById('error-message').innerHTML = errorMessage;
        }

        switch (count) {
            case 1:
                document.getElementById('message').style.height = '25px';
                document.getElementById('form').style.height = '450px';
                break;
            case 2:
                document.getElementById('message').style.height = '50px';
                document.getElementById('form').style.height = '500px';
                break;
            case 3:
                document.getElementById('message').style.height = '75px';
                document.getElementById('form').style.height = '530px';
                break;
            case 4:
                document.getElementById('message').style.height = '100px';
                document.getElementById('form').style.height = '550px';
                break;
            default:
                break;
        }
    })

    function validateForm() {
        const khoa = document.getElementById('Department').value;
        return khoa !== '';
    }
</script>
</body>
</html>