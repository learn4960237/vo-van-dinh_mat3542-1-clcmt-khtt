<!DOCTYPE html>
<html lang="en">
<head>
    <title>Danh sách sinh viên</title>
    <link rel="stylesheet" type="text/css" href="index_list.css">
    <style>
        td, th {
            border: 1px solid #000000;
            text-align: center;
            padding: 8px;
        }
    </style>
</head>
<body>
<form id="form" class="list" method="post" enctype="multipart/form-data">
    <div class="element-search">
        <label class="text-label"> Khoa </label>
        <label>
            <select class="search" id="select-option">
                <option id="option"></option>
            </select>
        </label>
        <div class="position-key">
            <label class="text-label">
                Từ khóa
            </label>
            <label>
                <input type="text" id="search-text" class="search" placeholder="Nhập từ khóa">
            </label>
        </div>
        <button type="button" onclick="searchData()" class="button"> Tìm kiếm</button>
    </div>
    <div class="element-display">
        <span>
            Số sinh viên tìm thấy: <span id="count"></span>
        </span>
        <a href="register.php">
            <input type="button" id="button-add" class="button" value="Thêm">
        </a>
    </div>
    <div class="element-table">
        <table id="table" class="table">
            <thead>
            <tr>
                <th>No</th>
                <th>Tên sinh viên</th>
                <th>Khoa</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody id="resultBody">

            </tbody>
        </table>
    </div>

</form>

<script>

    function loadAllData() {
        var table = document.getElementById("resultBody");
        table.innerHTML = ""; // Xóa nội dung hiện tại của bảng

        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                var data = JSON.parse(xhr.responseText);
                data.forEach(function (row, index) {
                    let count = table.rows.length;
                    document.getElementById('count').innerHTML = count + 1;
                    let newRow = table.insertRow(table.rows.length);
                    let cell1 = newRow.insertCell(0);
                    let cell2 = newRow.insertCell(1);
                    let cell3 = newRow.insertCell(2);
                    let cell4 = newRow.insertCell(3);
                    cell1.innerHTML = index + 1;
                    cell2.innerHTML = row.HoTen;
                    cell3.innerHTML = row.PhanKhoa;
                    // cell4.innerHTML = "<td><input type='button' id='button-delete' class='button-action' value='Xóa'> <button class='button-action'>Sửa</button></td></td>";
                    // console.log(row.HoTen)
                    // let Button_Detele = document.getElementById('button-delete');
                    // Button_Detele.addEventListener('click', function (){
                    //     DeleteData(row.HoTen)
                    // })
                    let deleteButton = document.createElement('button');
                    deleteButton.textContent = 'Xóa';
                    deleteButton.setAttribute('class', 'button-action');
                    deleteButton.onclick = function() {
                        DeleteData(row.HoTen); // Gọi hàm xóa dữ liệu với thông tin tương ứng
                        console.log(row.HoTen)
                    };

                    let editButton = document.createElement('button');
                    editButton.textContent = 'Sửa';
                    editButton.setAttribute('class', 'button-action');
                    editButton.onclick = function() {
                        // Hành động khi nhấp vào nút 'Sửa', tùy thuộc vào yêu cầu của bạn
                    };

                    cell4.appendChild(deleteButton);
                    cell4.appendChild(editButton);

                });
            }
        };
        xhr.open("GET", "database.php?action=loadAllData", true);
        xhr.send();
    }

    function searchData() {
        const keyword = document.getElementById("search-text").value;
        const valueOption = document.getElementById('select-option').value;
        const table = document.getElementById("resultBody");
        table.innerHTML = ""; // Xóa nội dung hiện tại của bảng
        // Gửi yêu cầu tìm kiếm bằng AJAX
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                data.forEach(function (row, index) {
                    let count = table.rows.length;
                    document.getElementById('count').innerHTML = count + 1;
                    let newRow = table.insertRow(table.rows.length);
                    let cell1 = newRow.insertCell(0);
                    let cell2 = newRow.insertCell(1);
                    let cell3 = newRow.insertCell(2);
                    let cell4 = newRow.insertCell(3);
                    cell1.innerHTML = index + 1;
                    cell2.innerHTML = row.HoTen;
                    cell3.innerHTML = row.PhanKhoa;
                    cell4.innerHTML = "<td><button onclick='DeleteData()' class='button-action'>Xóa</button> <button class='button-action'>Sửa</button></td></td>";
                });
            }
        };
        var url = "database.php";
        var params = new URLSearchParams();

        params.append("action", "search");
        params.append("keyword", keyword);
        params.append("valueOption", valueOption);

        url += "?" + params.toString();
        xhr.open("GET", url, true);
        xhr.send();
    }

    function loadFieldDataSelect() {
        let xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {
                let data = JSON.parse(xhr.responseText);
                // Lấy phần tử select từ DOM
                let selectElement = document.getElementById("select-option"); // Thay thế "yourSelectElementId" bằng ID thực tế của phần tử select

                // Sử dụng Set để theo dõi các giá trị đã xuất hiện
                let uniqueValues = new Set();

                // Xóa tất cả các tùy chọn hiện có trong phần tử select (nếu có)
                selectElement.innerHTML = '';
                let defaultOption = document.createElement("option");
                defaultOption.value = "";
                defaultOption.text = "Chọn khoa";
                defaultOption.disabled = true;
                defaultOption.selected = true;
                defaultOption.hidden = true;
                selectElement.appendChild(defaultOption);
                // Thêm tùy chọn từ dữ liệu vào phần tử select
                data.forEach(function(option) {
                    if (!uniqueValues.has(option.PhanKhoa)) {
                        let optionElement = document.createElement("option");
                        optionElement.value = option.PhanKhoa;
                        optionElement.text = option.PhanKhoa;
                        selectElement.appendChild(optionElement);
                        uniqueValues.add(option.PhanKhoa);
                    }
                });

            }
        }

        xhr.open("GET", "database.php?action=loadFieldDataSelect", true);
        xhr.send();
    }

    function DeleteData($ten) {
        let xhr = new XMLHttpRequest();
        console.log('a',$ten)
        xhr.open("GET","database.php?action=delete&$ten=" + $ten, true);
        xhr.send()
    }
    function loadPage() {
        loadFieldDataSelect();
        loadAllData();
        DeleteData();
    }

    window.onload = loadPage;
</script>
</body>
</html>
