#CREATE DATABASE ltweb CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE `student` (
    `Id` INT(100),
    `HoTen` varchar(30),
    `GioiTinh` varchar(10),
    `PhanKhoa` varchar(30),
    `NgaySinh` DATE,
    `DiaChi` varchar(100),
    `HinhAnh` BLOB
);

ALTER TABLE `student` ADD STT INT AUTO_INCREMENT PRIMARY KEY

