<!DOCTYPE html>
<html lang="en">
<head>
    <title>Xác nhận thông tin đăng ký</title>
    <link rel="stylesheet" href="index.css" type="text/css">
</head>
<body>

<form class="submit-form" id=" submit_form"  method="post">
    <div class="center-form">
        <div class="check">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo '<input type="text" class="date" " name="name" value="' . $_POST["ho_ten"] . '" ' . '>';
                ?>

            </label>
        </div>
        <div class="check">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo '<input type="text" class="date" " name="gioi_tinh" value="' . $_POST["gioi_tinh"] . '" ' . '>';
                ?>

            </label>
        </div>

        <div class="check">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label class="font">
                <?php
                    $year = $_POST['year'];
                    $month= $_POST['month'];
                    $day = $_POST['day'];
                    echo $day."/".$month."/".$year;
                ?>
            </label>
        </div>
        <div class="check">
            <label class="label"> Phân Khoa <span class="validate">*</span> </label>
            <label class="font">

            </label>
        </div>
        <div class="check">
            <label class="label"> Thông tin khác <span class="validate">*</span> </label>
            <label class="font">
                <?php
                echo '<input type="text" class="date" " name="address" value="' . $_POST["address"] . '" ' . '>';
                ?>
            </label>
        </div>

        <div class="center-btn">
            <input type="submit" id="submit" class="submit-btn" value="Xác nhận">
        </div>
    </div>
</form>
</body>
</html>
