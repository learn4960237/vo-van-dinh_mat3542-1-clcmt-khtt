<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Login</title>
    <link rel="stylesheet" type="text/css" href="index.css">
</head>
<body>
<form class="form" id="form" onsubmit="return validateForm()" action="registStudent.php"  method="post" enctype="multipart/form-data">
    <h2 style="text-align: center"> Form đăng ký sinh viên</h2>
    <div class="message" id="message">
        <p id="error-messages" class="error-message"></p>
    </div>
    <div class="center-form">
        <div class="table">
            <label class="label"> Họ và Tên <span class="validate">*</span> </label>
            <label>
                <input type="text" id="ho_ten" name="ho_ten" class="input">
            </label>
        </div>
        <div class="table">
            <label class="label"> Giới tính <span class="validate">*</span> </label>
            <div class="radio">
                <?php
                $gioi_tinh = array(0 => "Nam", 1 => "Nữ");
                foreach ($gioi_tinh as $key => $value) {
                    echo '<input type="radio" id="gioi_tinh"' . $key . '" name="gioi_tinh" value="' . $value . '" ' . '>';
                    echo '<label for="gioi_tinh_' . $key . '">' . $value . '</label>';
                }
                ?>
            </div>
        </div>

        <div class="table">
            <label class="label"> Ngày sinh <span class="validate">*</span> </label>
            <label >
                <select id="namSinh" class="birthday" name="year"></select>
                <select id="thangSinh" class="birthday" name="month">
                    <?php
                    $month = array();
                    for ($i = 1; $i <= 12; $i++) {
                        $month[] = $i;
                    }
                    foreach ($month as $value) {
                        echo '<option value= "' . $value . '">' . $value . '</option>';
                    }
                    ?>
                </select>
                <select id="ngaySinh" class="birthday" name="day">
                    <?php
                    $day = array();
                    for ($i = 1; $i <= 31; $i++) {
                        $day[] = $i;
                    }
                    foreach ($day as $value) {
                        echo '<option value= "' . $value . '">' . $value . '</option>';
                    }
                    ?>
                </select>
            </label>
        </div>
        <div class="table">
            <label class="label"> Địa chỉ <span class="validate">*</span> </label>
            <label>
                <select name="City" class="City" id="City" onchange="getQuan()">
                    <option disabled selected hidden value=""> Thành phố</option>
                    <option value="HN">Hà Nội</option>
                    <option value="HCM">TP. Hồ Chí Minh</option>
                </select>
                <select name="District" class="City" id="District">

                </select>
            </label>
        </div>
        <div class="table">
            <label class="label"> Thông tin khác </label>
            <label>
                <textarea class="address" id="address" name="address"> </textarea>
            </label>
        </div>
        <div class="center-btn">
            <input type="submit" id="submit-form" name="submit" class="submit-btn" value="Đăng Ký">
        </div>
    </div>
</form>
<script>
    document.getElementById("submit-form").addEventListener('click', function () {

        const hoTen = document.getElementById('ho_ten').value;
        const gioiTinh = document.querySelector('input[name="gioi_tinh"]:checked');
        let count = 0;
        let errorMessage = '';
        let errorFlag = false;

        if (hoTen === '') {
            errorMessage += 'Hãy nhập họ tên.<br>';
            errorFlag = true;
            count += 1;
        }

        if (!gioiTinh) {
            errorMessage += 'Hãy chọn giới tính.<br>';
            errorFlag = true;
            count += 1;
        }

        if (errorFlag) {
            document.getElementById('error-messages').innerHTML = errorMessage;
        }
        console.log(errorMessage)
        switch (count) {
            case 1:
                document.getElementById('message').style.height = '18px';
                document.getElementById('form').style.height = '500px';
                break;
            case 2:
                document.getElementById('message').style.height = '40px';
                document.getElementById('form').style.height = '530px';
                break;
            case 3:
                document.getElementById('message').style.height = '65px';
                document.getElementById('form').style.height = '560px';
                break;
            case 4:
                document.getElementById('message').style.height = '70px';
                document.getElementById('form').style.height = '580px';
                break;
            default:
                break;
        }
    })

    function validateForm() {
        const hoTen = document.getElementById('ho_ten').value;
        const gioiTinh = document.querySelector('input[name="gioi_tinh"]:checked');

        if(hoTen === ''|| !gioiTinh ){
            return false;
        }
    }

    var namSinh = document.getElementById('namSinh');
    var namHienTai = new Date().getFullYear();
    for (var i = namHienTai - 15; i >= namHienTai - 40; i--) {
        var option = document.createElement('option');
        option.value = i;
        option.text = i;
        namSinh.add(option);
    }
    function getQuan() {
        const DistrictData = {
            HCM: ["Quận 1", "Quận 2", "Quận 3","Quận 7", "Quận 9"],
            HN: ["Hoàng Mai", "Thanh Trì", "Nam Từ Liêm", "Hà Đông", "Cầu Giấy"]
        };
        const city = document.getElementById("City");
        const District = document.getElementById("District");
        const selectedCity = city.value;

        District.innerHTML = "";

        DistrictData[selectedCity].forEach(function (DistrictName) {
            let option = document.createElement("option");
            option.text = DistrictName;
            District.add(option);
        });
    }

</script>
</body>
</html>