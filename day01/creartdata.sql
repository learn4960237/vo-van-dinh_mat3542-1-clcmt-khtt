CREATE TABLE `dmkhoa` (
                          `MaKH` varchar(16) ,
                          `TenKhoa` varchar(30)
);


CREATE TABLE `sinhvien` (
                            `MaSV` varchar(6) ,
                            `HoSV` varchar(30),
                            `TenSV` varchar(15) ,
                            `GioiTinh` char(1) ,
                            `NgaySinh` datetime ,
                            `NoiSinh` varchar(50)  ,
                            `DiaChi` varchar(50) ,
                            `MaKH` varchar(16),
                            `HocBong` int(11)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

